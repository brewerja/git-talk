SOURCE_DOCS := $(wildcard *.md)
EXPORTED_DOCS := $(SOURCE_DOCS:.md=.html)

%.html: %.md
	landslide $< --relative --copy-theme -d $@

all: $(EXPORTED_DOCS)

clean:
	rm $(EXPORTED_DOCS)
	rm -rf theme
