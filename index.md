# Git Workshop

![comic](https://imgs.xkcd.com/comics/git.png "XKCD")

---

# Why use version control?

Because you're a professional.

Yes, really, it's that simple.

---

# Distributed Version Control

`git clone https://gitlab.com/bti360/workshops/git-katas.git`

"Where the complete codebase - including its full history - is mirrored on every developer's computer."

<img src="https://git-scm.com/book/en/v2/images/distributed.png" width="375" />

---

# The Basics

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/basic-commits/README.md" target="_blank">Kata: Basic Commits</a>

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/basic-staging/README.md" target="_blank">Kata: Basic Staging</a>

<br/>
<img src="images/simple-commit.png" />

---

# Branches: "The killer feature"

A branch is simply a lightweight pointer to a commit!

<a href="http://git-school.github.io/visualizing-git/" target="_blank">Visualizing Git</a>

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/basic-branching/README.md" target="_blank">Kata: Basic Branching</a>

---

# How to Use Branches

Always do your work in a branch (it's safe!).

Submit a merge request when your task is complete.

You will likely deploy from `master`.

<img src="images/merge-request.png" />

Small commits and short-lived branches are best.

---

# Merging

The `merge` command is used to integrate changes from another branch.

The target of this integration (i.e. the branch that receives changes) is always the currently checked out branch (`HEAD`).

To merge a feature branch into master, switch to the master branch, then call for the merge:

`git checkout master`

`git merge feature`

---

# Fast-forward Merge

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/ff-merge/README.md" target="_blank">Kata: Fast Forward Merge</a>

<img src="images/ff-merge.png" />

`git checkout master`

`git merge hotfix`

---

# Three-way Merge

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/3-way-merge/README.md" target="_blank">Kata: Three Way Merge</a>
<br/><br/>
<img src="images/git-merge-after.png" />

`git checkout master`

`git merge iss53`

---

# Merge Conflicts

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/merge-conflict/README.md" target="_blank">Kata: Merge Conflicts</a>

---

# Git Object Model

<img src="images/object-model.png" />

---

# Simple Commit Example

<img src="images/git-draw-tree.png" height="550"/>

---

<img src="images/listing.png" />

<img src="images/terminal.png" />

---

# Remotes

Consider that until now, everything except the initial `clone` has been local!

To share changes and incorporate the changes of others, you use remotes.

`git remote`

`git remote add <remote> <location>`

`git remote show <remote>`

---

# Fetch

To fetch changes from a remote:

`git fetch <remote>`

## Remote-Tracking Branches

Each time you do this, your local repository updates references to the state of the branches on that remote.

These are essentially "read only" branches of the form: `<remote>/<branch>`

NOTE: This does not update your working copy.

# Presenter Notes
`clone` does a lot:
 - creates a remote called `origin`
 - creates all remote-tracking branches
 - `master` branch set to track upstream `origin/master`

---

# So what does pull do?

`git pull == git fetch && git merge`

"Generally it’s better to simply use the fetch and merge commands explicitly as the magic of git pull can often be confusing."

Extra credit, but very useful: `git pull --rebase`

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/forked-repo-update/README.md" target="_blank">Kata: Remotes</a>

---

# Push

To share changes:

`git push <remote> <branch>`

<a href="https://gitlab.com/bti360/workshops/git-katas/blob/master/push/README.md" target="_blank">Kata: Push</a>

---

# Tracking Branches (Extra)

These are local branches with a direct relationship to an "upstream" remote branch.

`git checkout <branch>` where branch is same name as a remote tracking branch, creates an upstream relationship.

Another way to create an upstream relationship is with the `-u` flag:

`git push -u <remote> <branch>`

To list all local branches and denote those that are tracking the remote:

`git branch -vv`

---

# Helpful Links

 - <a href="http://rogerdudler.github.io/git-guide/" target="_blank">Git Simple Guide</a>
 - <a href="https://git-scm.com/book/en/v2" target="_blank">The Git Book</a>
 - <a href="https://github.com/pluralsight/git-internals-pdf/releases/download/v2.0/peepcode-git.pdf" target="_blank">Git Internals Book</a>
 - <a href="https://youtu.be/ZDR433b0HJY" target="_blank">Scott Chacon Git Talk Video</a>
 - <a href="https://chris.beams.io/posts/git-commit/" target="_blank">How to Write a Git Commit Message</a>
 <br/><br/>
 <br/><br/>
 <img src="images/logo.png" width="500" />
